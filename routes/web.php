<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/admin', function () {
    return view('card');
})->middleware('auth');



Route::get('/dasboard', function () {
    return view('card');
});
//tags route
// Route::get('/tags', 'TagController@index');
// Route::get('/tags/create', 'TagController@create');
// Route::post('/tags', 'TagController@store');
// Route::get('/tags/{id}/edit', 'TagController@edit');
// Route::put('/tags/{id}', 'TagController@update');
// Route::Delete('/tags/{id}', 'TagController@destroy');
Route::resource('/tags', 'TagController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//Route::get('/tags', 'TagController@index');
//Route::get('/tags/create', 'TagController@create');

// Route Profile
// Route::get('profile', 'ProfileController@index');
// Route::get('profile/create', 'ProfileController@create');
// Route::post('profile', 'ProfileController@store');
// Route::get('profile/{id}/edit', 'ProfileController@edit');
// Route::put('profile/{id}', 'ProfileController@update');
// Route::delete('profile/{id}', 'ProfileController@desstroy');

Route::resource('/profile', 'ProfileController');
//Route::post('/tags', 'TagController@store');
//Route::get('/tags/{id}/edit', 'TagController@edit');
//Route::put('/tags/{id}', 'TagController@update');
//Route::Delete('/tags/{id}', 'TagController@destroy');
Route::get('/', 'ArticleController@lihat');
Route::get('/article/index', 'ArticleController@index');
Route::get('/article/create', 'ArticleController@create');
Route::post('/article/index', 'ArticleController@store');
Route::get('/article/{id}/edit', 'ArticleController@edit');
Route::put('/article/index/{id}', 'ArticleController@update');
Route::delete('/article/index/{id}', 'ArticleController@destroy');
Route::get('/article/{id}/show', 'ArticleController@show');
Route::post('/article/{id}/show', 'ArticleController@storeComment');

//Route baru
Route::get()

@extends('layouts/master')
@section('content')
<div class="text-center">
    <h1 class="h4 text-gray-900 mb-4">Tambah Tags</h1>

</div>
<div class="card shadow mb-4">
    <form method="post" action="{{ url('/tags')}}" style="margin: 10px 50px 10px 50px">
        @csrf
        <div class="form-group row">
            <div class="col-sm-4 mb-3 mb-sm-0">
                <input type="text" name="tag_name" class="form-control form-control-user" id="tag_name" placeholder="tag_name">
            </div>

        </div>


        <button value="submit" name="submit" id="submit" class="btn btn-primary ">
            Simpan
        </button>


    </form>
</div>
@endsection
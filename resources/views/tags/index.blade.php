@extends('layouts/master')

@section('content')



<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Tables</h1>
    <p> <a href="{{ url('tags/create')}}" class="mb-4 btn btn-primary">Add Tags</a></p>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Table Tags</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="50%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Nama Tags</th>
                            <th>Aksi</th>


                        </tr>
                    </thead>
                    @foreach($tags as $tag)
                    <tbody>
                        <tr>

                            <td>{{ $tag->tag_name }}</td>
                            <td><a href="/tags/{{ $tag->id}}/edit" class="btn btn-primary"><i class="fa fa-edit"></i></a>

                                <form action="/tags/{{ $tag->id}}" method="post" style="display: inline">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" value="delete" class="btn btn-danger "><i class="fa fa-trash"></i></button>
                                </form>
                            </td>

                        </tr>
                    </tbody>
                    @endforeach
                </table>
            </div>
        </div>
    </div>

</div>
@endsection
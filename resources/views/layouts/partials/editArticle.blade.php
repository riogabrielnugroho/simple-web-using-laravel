@extends('layouts.master')

@section('content')
<form action='/article/index/{{ $articles->id }}' method='POST'>
  @csrf
  @method('PUT')
  <div class="form-group">
    <label for="exampleInputEmail1">Judul</label>
    <input type="text" name="judul" class="form-control" id="judul" placeholder="Masukkan Judul" value="{{ $articles->judul }}">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Isi</label>
    <textarea class="form-control" name="isi" placeholder="Masukkan isi artikel" id="tinymceriobermano" rows="4" cols="50">{{ $articles->isi }}</textarea>
  </div>
  <div class="form-group">
    <label for="#">Tags</label>
    <select name="tag_id" class="form-control">
        @foreach($tags as $tags)
          @if($tags->id == $articles->tag_id)
            <option value="{{$tags->id}}" selected>{{$tags->tag_name}}</option>
          @else
            <option value="{{$tags->id}}">{{$tags->tag_name}}</option>
          @endif
        @endforeach
    </select>
  </div>
  <button type="submit" class="btn btn-primary">Update</button>
</form>
@endsection
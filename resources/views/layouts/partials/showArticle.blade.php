@extends('layouts.master')

@section('content')
        <div class="card w-75">
            <div class="card-body">
                <h3 class="card-title">{{ $articles->judul }}</h3>
                <h6 class="card-title">Tags: {{ $articles->tag->tag_name }}</h6>
                <p class="card-text">{!! $articles->isi !!}</p>
            </div>
            @foreach($comments as $key => $comments)

                @if($comments->article_id == $articles->id)
                    <div class="card w-75">
                        <div class="card-body">
                            <h5 class="card-title">Komentar</h5>
                                <h6>{!! $comments->isi !!}</h6>
                        </div>
                    </div>
                @endif

            @endforeach
        </div>
        <div class="card w-75" style="margin-top:10px;">
            <form action="/article/{{ $articles->id }}/show" method="POST">
                @csrf
                <label for="#"><h6>Tambahkan Komentar</h6></label>
                <textarea class="form-control" name="isi" placeholder="Masukkan isi artikel" id="tinymceriobermano" rows="4" cols="50"></textarea>
                <button type="submit" class="btn btn-primary" style="margin-top:5px;">Submit</button>
            </form> 
        </div>
@endsection
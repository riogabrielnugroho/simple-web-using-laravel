@extends('layouts.master')

@section('content')
<form action='/article/index' method='POST'>
  @csrf
  <div class="form-group">
    <label for="#">Judul</label>
    <input type="text" name="judul" class="form-control" id="judul" placeholder="Masukkan Judul">
  </div>
  <div class="form-group">
    <label for="#">Isi</label>
    <textarea class="form-control" name="isi" placeholder="Masukkan isi artikel" id="tinymceriobermano" rows="4" cols="50"></textarea>
  </div>
  <div class="form-group">
    <label for="#">Tags</label>
    <select name="tag_id" class="form-control">
        @foreach($tags as $tags)
        <option value="{{$tags->id}}">{{$tags->tag_name}}</option>
        @endforeach
    </select>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
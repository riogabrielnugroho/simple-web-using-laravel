@extends('layouts.master')

@section('content')
<a href='/article/create'><button type="submit" class="btn btn-primary" style="margin-bottom:20px;">Create Article</button></a>
    @foreach($articles as $key => $articles)
    <div class="card w-75">
        <div class="card-body">
            <h3 class="card-title">{{ $articles->judul }}</h3>
            <h6 class="card-title">Tags: {{ $articles->tag->tag_name }}</h6>
            <p class="card-text">{!! $articles->isi !!}</p>
            <a href="/article/{{ $articles->id }}/show"><button type="submit" class="btn btn-primary" style="margin-bottom:20px;display:inline;"><i class="fa fa-edit"></i></button></a>
            <form action="/article/index/{{ $articles->id }}" method="POST" style="display:inline;">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger" style="margin-bottom:20px;"><i class="fa fa-trash"></i></button>
            </form>
            <a href="/article/{{ $articles->id }}/show"><button type="submit" class="btn btn-primary" style="margin-bottom:20px;display:inline;">Detail</button></a>
            <p>Created at: {{ $articles->created_at }} &nbsp; Updated at: {{ $articles->updated_at }}</p>
        </div>
    </div>
    @endforeach
@endsection
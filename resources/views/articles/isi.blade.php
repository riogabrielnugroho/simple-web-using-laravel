@extends('layouts/app')

@section('content')

    @foreach($articles as $key => $articles)
    <div class="container" style="margin-top:20px ">
        <div class="card w-75">
            <div class="card-body">
                <h3 class="card-title">{{ $articles->judul }}</h3>
                <p>Created at: {{ $articles->created_at }} &nbsp; Updated at: {{ $articles->updated_at }}</p>

                <p class="card-text">{!! $articles->isi !!}</p>
                <h6 class="card-title">Tags: {{ $articles->tag->tag_name }}</h6>
                <button type="submit" class="fa fa-thumbs-up" name="like"></button>
                    <form class="form-group" style="margin-top:20px " method="post" action="">
                        @csrf
                        <textarea type="text" rowspan="3" class="form-control col-md-6" name="comment" placeholder="comment"></textarea>
                        <button type="submit">koment</button>
                    </form>

            </div>
        </div>
        @foreach($comments as $key => $comments)

            @if($comments->article_id == $articles->id)
            <div class="card w-75">
                <div class="card-body">
                    <h5 class="card-title">Komentar</h5>
                        <h6>{!! $comments->isi !!}</h6>
                </div>
            </div>
            @endif

        @endforeach
    </div>
    @endforeach
@endsection
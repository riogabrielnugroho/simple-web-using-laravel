@extends('layouts.master')

@section('content')
<form action="/profile/{{$profiles->id}}" method="POST">
    @csrf
    @method('PUT')
        <div class="form-group">
        <label for="nama_lengkap">Nama Lengkap</label>
            <input type="text" class="form-control" placeholder="Enter your name" name="nama_lengkap" id="nama_lengkap" value="{{$profiles->nama_lengkap}}">
        </div>
        <div class="form-group">
            <label for="tgl_lahir">Tanggal Lahir</label>
            <input type="date" class="form-control" placeholder="Enter your birthday" name="tgl_lahir" id="tgl_lahir" value="{{$profiles->tgl_lahir}}">
        </div>
        <div class="form-group">
            <label for="tempat_lahir">Tempat Lahir</label>
            <input type="text" class="form-control" placeholder="Enter your place birth" name="tempat_lahir" id="tempat_lahir" value="{{$profiles->tempat_lahir}}">
        </div>
        <div class="form-group">
            <label for="alamat">Alamat</label>
            <input type="text" class="form-control" placeholder="Enter your address" name="alamat" id="alamat" value="{{$profiles->alamat}}">
        </div>
        <div class="form-group">
            <label for="jk">Jenis Kelamin</label>
            <input type="enum" class="form-control" placeholder="Enter your address" name="jk" id="jk" value="{{$profiles->jk}}"> 
        </div>
            <button type="submit" class="btn btn-primary">Update</button>
    </form>
@endsection
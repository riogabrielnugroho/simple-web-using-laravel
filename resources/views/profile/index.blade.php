@extends('layouts.master')

@section('content')

<a href="{{ route('profile.create')}}" class="btn btn-primary mb-3">Add profile</a>
@foreach($profiles as $profile)
<div class="container mt-3">
    <div class="row">
        <div class="col-5">
            <div id="accordion">
                <div class="card">
                  <div class="card-header">
                            <a href="{{ route('profile.edit', ['profile' => $profile->id]) }}">
                                <button class="btn btn-success btn-xs"> <i class="fa fa-edit"></i></button>
                            </a>
                            <form action="{{ route('profile.destroy', ['profile' => $profile->id]) }}" method="post" style="display:inline">
                                @csrf
                                @method('DELETE')

                                    <button type="submit" class="btn btn-danger btn-xs" value="delete"> <i class="fa fa-trash"></i> </button>
                  </div>
                
                  <div id="collapseOne" class="collapse show" data-parent="#accordion">
                    <div class="card-body">
                        
                    
                        <p><i class="fas fa-user-tie"></i> <span class="font-weight-bold" name="nama_lengkap" value="nama_lengkap">{{ $profile->nama_lengkap }}</span></p>
                        <p><i class="fas fa-calendar-alt"></i> <span class="font-weight-bold" name="tgl_lahir" value="tgl_lahir">{{ $profile->tgl_lahir }}</span></p>
                        <p><i class="fas fa-address-card"></i> <span class="font-weight-bold" name="tempat_lahir" value="tempat_lahir">{{ $profile->tempat_lahir }}</span></p>
                        <p><i class="fas fa-map-marker-alt"></i> <span class="font-weight-bold" name="alamat" value="alamat">{{ $profile->alamat }}</span></p>
                        <p><i class="fas fa-venus-mars"></i> <span class="font-weight-bold" name="jk" value="jk">{{ $profile->jk }}</span></p>
                    </div>
                  </div>
                  <div class="card-footer text-center">
                      <i class="fab fa-facebook-square"></i>
                      <i class="fab fa-twitter-square"></i>
                      <i class="fab fa-linkedin"></i>
                      <i class="fab fa-youtube-square"></i>
                      <i class="fab fa-instagram"></i>
                  </div>
                  
                </div>
              </div>
        </div>
    </div>
</div>
@endforeach

@endsection
@extends('layouts.master')

@section('content')

<form action="{{ route('profile.store') }}" method="POST">
    @csrf
    <div class="form-group">
        <label for="nama_lengkap">Nama Lengkap</label>
        <input type="text" class="form-control" placeholder="Enter your name" name="nama_lengkap" id="nama_lengkap">
    </div>
    <div class="form-group">
        <label for="tgl_lahir">Tanggal Lahir</label>
        <input type="date" class="form-control" placeholder="Enter your birthday" name="tgl_lahir" id="tgl_lahir">
    </div>
    <div class="form-group">
        <label for="tempat_lahir">Tempat Lahir</label>
        <input type="text" class="form-control" placeholder="Enter your place birth" name="tempat_lahir" id="tempat_lahir">
    </div>
    <div class="form-group">
        <label for="alamat">Alamat</label>
        <input type="text" class="form-control" placeholder="Enter your address" name="alamat" id="alamat">
    </div>
    <div class="form-group">
        <label for="jk">Jenis Kelamin</label>
        <div class="form-check form-inline">
            <input type="radio" name="jk" value="P" class="form-check-input" />
            Wanita
            <input type="radio" name="jk" value="L" class="form-check-input" />
            Pria
        </div>
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
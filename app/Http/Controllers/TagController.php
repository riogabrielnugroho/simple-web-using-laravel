<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tag;
use PhpParser\Node\Stmt\Return_;

class TagController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $tags = Tag::all();

        return view('/tags/index', ["tags" => $tags]);
    }
    public function create()
    {
        return view('/tags/create');
    }
    public function store(Request $request)
    {
        //dd($request->all());
        $tags = new Tag();
        $tags->tag_name = $request['tag_name'];
        $tags->save();
        return redirect('/tags');
    }
    public function edit($id)
    {
        $tags = Tag::findOrFail($id);
        return view('/tags/edit', ["tag" => $tags]);
    }
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $tags = Tag::findOrFail($id);
        $tags->tag_name = $request['tag_name'];
        $tags->save();
        return redirect('/tags');
    }
    public function destroy($id)
    {
        Tag::destroy($id);
        return redirect('/tags');
    }
}

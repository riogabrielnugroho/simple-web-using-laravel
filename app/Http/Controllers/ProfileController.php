<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Profile;
use App\User;


class ProfileController extends Controller
{ ///untu auth penjagaan seluruh controler profile
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $profiles = Profile::all();

        return view('profile.index', ["profiles" => $profiles]);
    }

    public function create()
    {
        $users = User::all();

        return view('profile.create', compact('users'));
    }
    public function store(Request $request)
    {
        dd($request->all());
        //create cara biasa
        $new_profile = new Profile();
        $new_profile->nama_lengkap = $request["nama_lengkap"];
        $new_profile->tgl_lahir = $request["tgl_lahir"];
        $new_profile->tempat_lahir = $request["tempat_lahir"];
        $new_profile->alamat = $request["alamat"];
        $new_profile->jk = $request["jk"];
        $new_profile->save();

        return redirect('/profile');
    }

    public function edit($id)
    {
        $profiles = Profile::findOrFail($id);
        //dd($profiles->all());

        return view('profile.edit', ["profiles" => $profiles]);
    }

    public function update(Request $request, $id)
    {
        //cari data item dengan id = $id
        //ubah2 datanya dengan request yang masuk dari form
        //save()

        $new_profile = Profile::findOrFail($id);

        $new_profile->nama_lengkap = $request["nama_lengkap"];
        $new_profile->tgl_lahir = $request["tgl_lahir"];
        $new_profile->tempat_lahir = $request["tempat_lahir"];
        $new_profile->alamat = $request["alamat"];
        $new_profile->jk = $request["jk"];
        $new_profile->save();

        return redirect('/profile');
    }

    public function destroy($id)
    {
        Profile::destroy($id);

        return redirect('/profile');
    }
}

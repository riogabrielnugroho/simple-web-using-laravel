<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Tag;
use App\Comment;

class ArticleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('lihat');
    }
    public function index()
    {
        $articles = Article::all();

        return view('layouts.partials.indexArticle', ["articles" => $articles]);
    }
    public function lihat()
    {
        $articles = Article::all();
        $comments = Comment::all();
        return view('articles.isi', ["articles" => $articles], ["comments" =>$comments]);
    }

    public function create()
    {
        $tags = Tag::all();

        return view('layouts.partials.createArticle', ["tags" => $tags]);
    }

    public function store(Request $request)
    {
        $new_articles = new Article();
        $new_articles->judul = $request["judul"];
        $new_articles->isi = $request["isi"];
        $new_articles->tag_id = $request["tag_id"];
        // $new_articles->update(['creation_date' => now()]);
        // $new_articles->update(['updated_at' => now()]);
        $new_articles->save();

        return redirect('/article/index');
    }

    public function edit($id)
    {
        $articles = Article::findOrFail($id);
        $tags = Tag::all();
        return view('layouts.partials.editArticle', ['articles'=>$articles , 'tags' => $tags]);
    }

    public function update(Request $request, $id)
    {
        $new_articles = Article::findOrFail($id);
        $new_articles->judul = $request["judul"];
        $new_articles->isi = $request["isi"];
        $new_articles->tag_id = $request["tag_id"];
        $new_articles->save();

        return redirect('/article/index');
    }

    public function destroy($id)
    {
        Article::destroy($id);
        return redirect('/article/index');
    }

    public function show($id){
        $articles = Article::findOrFail($id);
        $comments = Comment::all();
        return view('layouts.partials.showArticle', ['articles'=>$articles, 'comments' =>$comments]);
    }

    public function storeComment(Request $request, $id){
        $new_comments = new Comment();
        $new_comments->isi = $request["isi"];
        $new_comments->article_id = $id;
        $new_comments->save();

        return redirect('/article/{id}/show');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    // protected $guarded = [];
    public function tag(){
        return $this->belongsTo('App\Tag');
    }

    public function comment()
    {
        return $this->hasMany('App\Comment');
    }
}
